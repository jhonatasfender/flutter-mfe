# Tutorial Teórico sobre Get_it - Injeção de Dependência no Flutter

## Introdução

A injeção de dependência (DI) é uma prática de programação que visa facilitar a manutenção e teste de software, reduzindo o acoplamento entre diferentes partes do código. O `get_it` é uma biblioteca popular para a implementação de injeção de dependência no Flutter e no Dart.

## O que é Get_it?

`get_it` é uma biblioteca Dart/Flutter para gerenciamento de dependências que fornece um Service Locator para injetar dependências de forma eficiente em toda a aplicação. Ele permite registrar, recuperar e fornecer instâncias de objetos de forma fácil.

## Instalação

Adicione o `get_it` ao seu arquivo `pubspec.yaml`:

```yaml
dependencies:
  get_it: ^7.1.4
```

Depois, execute `flutter pub get` no terminal para baixar e instalar a biblioteca.

## Uso Básico

### 1. Importação

```dart
import 'package:get_it/get_it.dart';
```

### 2. Registro de Dependências

```dart
GetIt getIt = GetIt.instance;

getIt.registerSingleton<MyService>(MyService());
getIt.registerFactory(() => AnotherService());
```

- `registerSingleton`: Registra um serviço como um singleton. A instância é criada apenas uma vez e reutilizada sempre que solicitada.
- `registerFactory`: Registra uma fábrica para criar instâncias sempre que solicitado.

### 3. Recuperação de Dependências

```dart
MyService myService = getIt<MyService>();
AnotherService anotherService = getIt<AnotherService>();
```

- `getIt<T>()`: Recupera a instância registrada para o tipo `T`.

### 4. Uso no Flutter

Em um widget, por exemplo, você pode acessar as dependências no método `build`:

```dart
class MyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MyService myService = getIt<MyService>();
    return Text(myService.someMethod());
  }
}
```

## Injeção de Dependência Lazy

Às vezes, você deseja que a instância de um objeto seja criada apenas quando for necessária. O `get_it` suporta isso usando `registerLazySingleton`:

```dart
getIt.registerLazySingleton<MyLazyService>(() => MyLazyService());
```

Certamente, vou refazer essa parte do tutorial para fornecer uma explicação mais clara:

## Callbacks e `Inject`

Ao registrar uma dependência utilizando o `get_it`, você tem a opção de fornecer um callback que será invocado sempre que alguém solicitar uma instância do tipo que você está registrando. Este callback recebe um parâmetro chamado `inject`, que é uma instância da classe `Inject`. O `Inject` contém informações úteis sobre o contexto da injeção de dependência.

Vamos explorar um exemplo mais detalhado usando uma classe `MyService` que depende de outro serviço chamado `DependencyService`:

```dart
class DependencyService {
  String dependencyName;

  DependencyService(this.dependencyName);

  void doSomethingElse() {
    print("DependencyService ($dependencyName) is doing something else!");
  }
}

class MyService {
  String name;
  DependencyService dependencyService;

  MyService(this.name, this.dependencyService);

  void doSomething() {
    print("MyService ($name) is doing something!");
    dependencyService.doSomethingElse();
  }
}
```

Agora, ao registrar `MyService` com um callback, você pode personalizar a criação da instância e injetar as dependências necessárias:

```dart
getIt.registerFactory<MyService>((inject) {
  // Acessar informações sobre a injeção
  print("Tipo da Injeção: ${inject.type}");
  print("Origem da Injeção: ${inject.origin}");

  // Personalizar a criação com base nas informações da injeção
  String name = "Default";
  if (inject.hasParam('name')) {
    name = inject.param('name', 'Default');
  }

  // Injetar a dependência no construtor de MyService
  return MyService(name, getIt<DependencyService>());
});
```

- O callback agora acessa informações sobre a injeção, como o tipo da instância solicitada (`inject.type`) e a origem da injeção (`inject.origin`).
  
- Personaliza a criação da instância, neste caso, ajusta o nome com base nos parâmetros fornecidos na solicitação.

Agora, quando você solicita uma instância de `MyService`, o `get_it` utiliza este callback para criar a instância personalizada levando em consideração as informações da injeção.

```dart
MyService myService = getIt<MyService>(param1: 'CustomName');
myService.doSomething();
```

Espero que isso torne mais claro como utilizar callbacks e o `Inject` para personalizar a injeção de dependência em situações mais complexas.

## Conclusão

O `get_it` é uma ferramenta poderosa para gerenciar dependências no Flutter e Dart, facilitando a criação de aplicativos mais modularizados, testáveis e escaláveis. Ao entender e utilizar corretamente a injeção de dependência, você pode melhorar a organização do seu código e facilitar o desenvolvimento e manutenção do seu aplicativo.
