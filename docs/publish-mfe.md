# Criando um Pacote Flutter para Publicação

Ao criar e publicar um pacote Flutter, você deve seguir algumas etapas essenciais. Vamos detalhar cada uma dessas etapas.

## 1. Criação do Pacote

Execute o comando a seguir para criar um novo pacote Flutter:

```bash
flutter create --template=package hello
```

Este comando cria a estrutura básica para o seu pacote.

## 2. Configuração do `pubspec.yaml`

O arquivo `pubspec.yaml` é central para a configuração do seu pacote. Vamos detalhar as principais configurações:

```yaml
name: hello
version: 0.0.1
description: Uma descrição do seu pacote
publish_to: 'none' # comente ou delete essa linha para publicação local ou ou adicione 'gitlab' para publicação no GitLab

environment:
  sdk: ">=2.12.0 <3.0.0"

dependencies:
  flutter:
    sdk: flutter

dev_dependencies:
  pedantic: ^1.0.0
  test: ^1.0.0

# Configuração específica para a publicação no GitLab
repository:
  url: git+https://gitlab.com/seu_usuario/seu_projeto.git
  type: git

# Outras configurações opcionais
homepage: https://exemplo.com
license: MIT
```

- **`name`:** Nome exclusivo do seu pacote no pub.dev.
  
- **`version`:** Versão atual do seu pacote, seguindo Semantic Versioning (SemVer).

- **`description`:** Uma breve descrição do pacote.

- **`publish_to`:** Essa configuração para aplicações localmente deverá ser removida ou comentado do arquivo; para os casos de publicações fora dos ambientes locais ajuste para o registro GitLab quando estiver pronto para publicar remotamente.

- **`environment`:** Especifica a versão mínima do SDK Flutter necessária.

- **`dependencies`:** Lista de dependências, incluindo o SDK Flutter.

- **`dev_dependencies`:** Dependências de desenvolvimento, como pacotes de teste.

- **`repository`:** Configuração específica para o GitLab.

- **`homepage`:** Opcional, mas recomendado. Um link para a página principal do pacote.

- **`license`:** Especifica a licença do seu pacote.

# Versionamento Semântico (SemVer) em Pacotes Flutter

O versionamento semântico (SemVer) é uma prática importante ao desenvolver pacotes Flutter. Ele ajuda a comunicar de maneira clara e consistente as mudanças feitas em um pacote, permitindo que os usuários compreendam o impacto das atualizações. Aqui estão alguns conceitos-chave relacionados ao versionamento semântico:

## O Formato da Versão

O número da versão de um pacote segue o formato X.Y.Z, onde:

- **X (Major):** A versão principal, indicando alterações incompatíveis com versões anteriores.
- **Y (Minor):** A versão secundária, indicando adições de funcionalidades de maneira compatível com versões anteriores.
- **Z (Patch):** A versão de correção, indicando correções de bugs de maneira compatível com versões anteriores.

## Pré-lançamentos (Prereleases)

Às vezes, é necessário disponibilizar versões de desenvolvimento ou pré-lançamentos para testes antes de uma versão estável. Isso é feito adicionando uma etiqueta de pré-lançamento ao número da versão, como -alpha, -beta, ou -rc (release candidate). Por exemplo:

- **1.0.0-alpha.1:** Uma versão alfa, indicando uma fase inicial de desenvolvimento.
- **1.0.0-beta.2:** Uma versão beta, indicando uma fase de desenvolvimento mais madura.
- **1.0.0-rc.3:** Um candidato a lançamento, indicando uma versão que pode se tornar estável se nenhum problema significativo for encontrado.

## Boas Práticas

Ao usar o versionamento semântico, aqui estão algumas boas práticas a serem consideradas:

1. **Incremento de Major (X):** Faça quando realizar alterações incompatíveis com versões anteriores, como grandes reescritas ou alterações de API que quebram a compatibilidade.

2. **Incremento de Minor (Y):** Faça quando adicionar novas funcionalidades de maneira compatível com versões anteriores.

3. **Incremento de Patch (Z):** Faça quando corrigir bugs de maneira compatível com versões anteriores.

4. **Pré-lançamentos:** Use etiquetas de pré-lançamento para indicar versões em desenvolvimento. Isso permite que os usuários experimentem as novas funcionalidades enquanto se preparam para a versão estável.

## Exemplo Prático no `pubspec.yaml`

Ao definir a versão no `pubspec.yaml`, você pode seguir estas diretrizes. Por exemplo, para uma versão de pré-lançamento:

```yaml
version: 1.0.0-beta.1
```

Ou para uma versão estável:

```yaml
version: 1.0.0
```

## 3. Login no GitLab

Antes de publicar no GitLab, faça login usando o comando:

```bash
flutter pub login --retry
```

## 4. Publicação Local (Simulação)

Para testar a publicação localmente, use o comando `flutter pub publish --dry-run`:

```bash
flutter pub publish --dry-run
```

O `--dry-run` simula a publicação sem efetuar alterações.

## 5. Visualizando Pacotes Publicados Localmente

Após o teste, os pacotes são armazenados localmente em `~/.pub-cache/hosted`. Visualize-os com:

```bash
cd ~/.pub-cache/hosted
ls
```

## 6. Arquivos Importantes no Projeto `hello`

1. **`analysis_options.yaml`:**
   - Configurações para linter e análise estática do Dart.

2. **`CHANGELOG.md`:**
   - Documenta alterações significativas em cada versão.

3. **`hello.iml`:**
   - Específico do IntelliJ IDEA para configurações do projeto.

4. **`lib/`:**
   - Contém o código-fonte principal. `hello.dart` tem a implementação.

5. **`LICENSE`:**
   - Define os termos de licença do pacote.

6. **`pubspec.lock`:**
   - Gerado automaticamente, contém versões específicas de dependências.

7. **`pubspec.yaml`:**
   - Configuração principal do projeto. Ajuste conforme necessário.

8. **`README.md`:**
   - Documentação de boas-vindas ao seu pacote.

9. **`test/`:**
   - Contém testes unitários.

## 7. Configurações Adicionais

- **`CHANGELOG.md`:**
   - Embora não obrigatório, mantenha um arquivo bem documentado para informar alterações em cada versão.

- **`LICENSE`:**
   - Inclua um arquivo de licença para definir os termos de uso do seu código.

Lembre-se, quanto mais detalhado e teórico o README e o CHANGELOG, melhor para os desenvolvedores entenderem e utilizarem seu pacote. Certifique-se de seguir as práticas de versionamento semântico para uma experiência consistente.