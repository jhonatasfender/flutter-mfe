# Usando o `pub.dev` Privado

Adicione o seguinte trecho ao seu arquivo `pubspec.yaml` para especificar o repositório privado:

```yaml
publish_to: '<URL_DO_REGISTRO_PRIVADO>'
```

Ao publicar um pacote, utilize o seguinte comando:

```bash
flutter pub publish 
```

Substitua `<URL_DO_REGISTRO_PRIVADO>` pelo URL real do seu repositório privado.

## Configurando um Servidor de Pub Privado com Unpub e MongoDB

### Introdução

O Unpub é uma excelente escolha se você deseja configurar um servidor de Pub privado para o Dart. Este servidor privado Dart Pub é destinado a empresas e possui uma interface web simples para procurar e visualizar informações sobre pacotes.

Neste tutorial, vou guiá-lo através dos passos para configurar um servidor Unpub usando o MongoDB como armazenamento de metadados.

### Pré-requisitos

Antes de começarmos, certifique-se de ter o MongoDB instalado e em execução na sua máquina ou em um servidor acessível. Você pode seguir as [instruções de instalação do MongoDB](https://docs.mongodb.com/manual/installation/) para configurar o MongoDB.

## Passo 1: Instalando o Unpub Server

Execute o seguinte comando para instalar o servidor Unpub:

```bash
flutter pub global activate unpub
```

Para realizar a autenticação e conseguir publicar, é necessário instalar esta lib:

```bash
flutter pub global activate unpub_auth
```

### Passo 2: Configurando o MongoDB

O Unpub utiliza o MongoDB como armazenamento para metadados. Certifique-se de criar um banco de dados no MongoDB para o Unpub. Vamos chamar o banco de dados de `dart_pub`. Você pode fazer isso usando o shell do MongoDB:

```bash
mongo
use dart_pub
```

### Passo 3: Iniciando o Servidor Unpub

Depois de instalar, inicie o servidor Unpub, apontando-o para o banco de dados MongoDB:

```bash
unpub --database mongodb://localhost:27017/dart_pub
```

Você também pode usar apenas `unpub`, pois por padrão já irá colocar essa URL. É mais para saber que tem essa possibilidade.

Isso iniciará o servidor Unpub, e você poderá acessar a interface web do Unpub no endereço `http://localhost:4000`. 

### Personalizando o Unpub

O Unpub é projetado para ser extensível. Você pode personalizar o armazenamento de metadados e o armazenamento de pacotes se necessário. No entanto, o uso padrão com MongoDB e armazenamento em arquivo é adequado para muitos casos.

### Configuração do Unpub com Docker

Se preferir usar o Docker para o MongoDB, você pode executar o seguinte comando para iniciar um contêiner MongoDB:

```bash
docker run -p 27017:27017 --name mongodb mongo
```

Em seguida, ao iniciar o Unpub, você pode apontá-lo para o contêiner MongoDB usando o IP da máquina host, algo assim:

```bash
unpub --database mongodb://host.docker.internal:27017/dart_pub
```

Isso é especialmente útil se você estiver executando o Docker em um sistema operacional que não seja Linux.

# Autenticação

Depois de ter feito todo o procedimento, você irá realizar a autenticação, utilizando o `unpub_auth`.

O comando será esse: `unpub_auth login http://localhost:4000`. Depois de executar esse comando, irá aparecer algo assim no terminal:

```bash
unpub needs your authorization to upload packages on your behalf.
In a web browser, go to https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=468492456239-2ckl1u0utih8tsekg0ligcicejbo0nvd.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A43230&code_challenge=KUQ23nvbRABTvNJTVjwiNO869-jjsxDA-nk9xLp8EnE&code_challenge_method=S256&scope=openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=offline&approval_prompt=force
Then click "Allow access".
```

Clique no link e realize a autenticação com alguma conta do Google. Depois disso, irá imprimir esse token no terminal:

```bash
Waiting for your authorization...Authorization received, processing...Successfully authorized.
ya29.a0AfB_byCFM_veV9UETrAkelOqqXAXPD-vHtcJOoK6qFXtdFQm9rMD7pPorEOzZDaBd_c3vONXX7wHjDCDUAsJnLmVau0mfbZ2NfXP6HnP3aqF0NOq7oy8YWny0JbWqhQpXSwpcKHpygHT1j1666cjmmCwUxw6waevn1MXaCgYKAcQSARISFQHGX2MivD3KEsfnMt6ifeqxpXDhZg0171
```

Este token irá aparecer caso tenha sucesso na autenticação. Logo em seguida, execute este comando para realizar o login no `unpub`:

```bash
flutter pub token add http://localhost:4000
```

Assim que executar este comando, irá solicitar o token dessa forma no terminal:

```bash
Enter secret token:
```

No comando anterior, irá pegar o token que avia mencionado acima, sem o `%` (esse símbolo não faz parte do token). Este

 token é o que você deve inserir.

Se fizer tudo correto, irá apresentar isso no terminal:

```bash
Requests to "http://localhost:4000" will now be authenticated using the secret token.
```

Feito isso, agora poderá executar o comando `flutter pub publish`.

É esperado que, ao executar o comando `flutter pub publish`, seja criada uma pasta chamada `unpub-packages` que conterá uma versão zipada do seu pacote. Este é um comportamento normal do processo de publicação de pacotes Flutter.

Essa pasta contém a versão publicada do seu pacote no formato zip, e é usada para armazenar os pacotes publicados localmente. Em um ambiente de produção, os pacotes seriam armazenados em um repositório de pacotes, como o Pub.dev ou um servidor Unpub configurado para ser acessível online.

O fato de o pacote estar sendo armazenado localmente em um diretório chamado `unpub-packages` não afeta diretamente a disponibilidade do pacote para download ou uso no seu ambiente local de desenvolvimento. O servidor Unpub deve ser configurado corretamente para lidar com a publicação e distribuição dos pacotes. Se houver problemas com a visualização dos pacotes no navegador ou na interface web do Unpub, é possível que o problema esteja relacionado à configuração do Unpub ou a problemas de conexão com o banco de dados MongoDB, conforme discutido anteriormente.

Para verificar se publicou, basta olhar essa pasta. Ao fazer a instalação, fique de olho no log do servidor do Unpub porque lá irá apresentar caso não tenha encontrado o caminho correto do package.

1. **Adicione o Repositório Privado ao `pubspec.yaml`:**
   No arquivo `pubspec.yaml` da sua nova aplicação, adicione o repositório privado Unpub como uma origem de pacotes. Certifique-se de substituir `http://localhost:4000` pelo URL correto do seu servidor Unpub.

   ```yaml
   dependencies:
     hello:
       hosted:
         name: hello
         url: http://localhost:4000
   ```

   Lembre-se de ajustar a versão do pacote conforme necessário.

2. **Execute o Comando `flutter pub get`:**
   Após adicionar o repositório privado ao `pubspec.yaml`, execute o comando `flutter pub get` no terminal. Isso baixará e instalará as dependências, incluindo o pacote que você publicou no Unpub.

   ```bash
   flutter pub get
   ```

   Certifique-se de que sua nova aplicação possa acessar o servidor Unpub. Se você estiver executando em um ambiente local, certifique-se de que ambos, o servidor Unpub e a nova aplicação, estejam na mesma rede.

3. **Utilize o Pacote Normalmente:**
   Agora, você pode usar o pacote normalmente em sua aplicação. Importe as classes e utilize as funcionalidades fornecidas pelo pacote.

   ```dart
   import 'package:hello/hello.dart';

   void main() {
     Hello.sayHello();
   }
   ```

   Lembre-se de que, se você tiver configurado autenticação para o Unpub, a nova aplicação também precisará ser autenticada para acessar o pacote.

4. **Configurar a Autenticação (Se Necessário):**
   Se o seu servidor Unpub requer autenticação, você precisa garantir que a nova aplicação esteja configurada para isso. Você pode usar o comando `flutter pub token add` para adicionar um token de acesso. Certifique-se de seguir os passos de autenticação necessários.

   ```bash
   flutter pub token add http://localhost:4000
   ```

   Após adicionar o token, a nova aplicação poderá acessar pacotes protegidos por autenticação no seu servidor Unpub.


### Conclusão

Agora você tem um servidor Unpub em execução que você pode usar como seu repositório privado de pacotes Dart. Este é um passo importante para quem trabalha em um ambiente corporativo ou precisa de um controle mais granular sobre os pacotes usados em seus projetos Dart. Certifique-se de verificar a [documentação oficial do Unpub](https://pub.dev/packages/unpub) para obter mais detalhes e opções de configuração avançadas.
