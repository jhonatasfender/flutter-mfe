abstract class Module01Repository {
  String fetchData();
}

class Module01RepositoryImpl implements Module01Repository {
  @override
  String fetchData() {
    return "Data from Module 01 Repository";
  }
}
