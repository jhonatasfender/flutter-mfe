import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'module_01_controller.dart';

class Module01Screen extends StatelessWidget {
  final Module01Controller _controller = GetIt.I<Module01Controller>();

  Module01Screen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Module 01 Screen"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(_controller.fetchData()),
            ElevatedButton(
              onPressed: () {},
              child: const Text("Action in Module 01"),
            ),
          ],
        ),
      ),
    );
  }
}
