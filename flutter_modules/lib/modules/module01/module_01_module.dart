import 'package:flutter_modular/flutter_modular.dart';
import 'package:get_it/get_it.dart';

import 'module_01_controller.dart';
import 'module_01_repository.dart';
import 'module_01_screen.dart';

class Module01Module extends Module {
  @override
  final List<Bind> binds = [
    // Repository
    Bind.lazySingleton<Module01Repository>((i) => Module01RepositoryImpl()),

    // Controller
    Bind.factory((i) => Module01Controller(i<Module01Repository>())),

    // Screen
    Bind.factory((i) => Module01Screen()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/module01', child: (_, args) => Module01Screen()),
  ];
}
