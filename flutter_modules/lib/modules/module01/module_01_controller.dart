import 'package:flutter_modules/modules/module01/module_01_repository.dart';

class Module01Controller {
  final Module01Repository _repository;

  Module01Controller(this._repository);

  String fetchData() {
    return _repository.fetchData();
  }
}
